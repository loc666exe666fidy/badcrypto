A simple crypto that is easy to break. I publish it as a challenge for
people to break.

# puzzle_ciphertext

An encrypted file that you can try to decrypt. The decrypted version
of this file contains only ascii characters. The content of the
original file isn't confidential.

# example files

example_plaintext.gz and example_ciphertext are an example file and
its encrypted version. This file was encrypted with the same key as
used for puzzle_ciphertext.

# Source files

In the src/ folder there are (obfuscated) sources for
encryption/decryption and key generation. It uses just python2 and no
non-standard libraries.

# Hints

This is a symmetric cipher with a 65,536 bit key. Trying every key
isn't feasable. The puzzle_ciphertext file is probably too short to be
cracked without the examples.

You can derive the key from the example files. I don't think breaking
the cipher is feasable by hand, however the original key can be
recovered in a fraction of a second using a (modern) computer.
